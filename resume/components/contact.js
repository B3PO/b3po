app.component('contact', {
    template: 
    /*html*/
    ` 
    <section class="cta-section theme-bg-light py-5">
    <div class="container text-center single-col-max-width">
        <h2 class="heading">Contact</h2>
        <div class="intro">
            <p>Interested in hiring me for your project? You can fill in the contact form below or send me an email to <a  class="text-link" href="mailto:#">{{email}}</a></p>
            <p>Want to get connected? Follow me below.</p>
            <ul class="list-inline mb-0">
                <li class="list-inline-item mb-3"><a class="twitter" href="https://twitter.com/B3p01"><i class="fab fa-twitter fa-fw fa-lg"></i></a></li>
                <li class="list-inline-item mb-3"><a class="linkedin" href="https://www.linkedin.com/in/brandon-rowe-56887791/"><i class="fab fa-linkedin-in fa-fw fa-lg"></i></a></li>
                <li class="list-inline-item mb-3"><a class="github" href="https://gitlab.com/B3PO"><i class="fab fa-gitlab fa-fw fa-lg"></i></a></li>
                <li class="list-inline-item mb-3"><a class="github" href="https://github.com/brandon-rowe"><i class="fab fa-github-alt fa-fw fa-lg"></i></a></li>
            </ul>
        </div>
    </div>
    </section>
    <section class="contact-section px-3 py-5 p-md-5">
        <div class="container">
            <form id="contact-form" class="contact-form col-lg-8 mx-lg-auto" @submit.prevent="sendForm">
                <h3 class="text-center mb-3">Get In Touch</h3>
                <div class="row g-3">                                                           
                    <div class="col-12 col-md-6">
                        <label class="sr-only" for="cname">Name</label>
                        <input id="cname" type="text" class="form-control" v-model="cname" name="name" placeholder="Name" minlength="2" required="" aria-required="true">
                    </div>                    
                    <div class="col-12 col-md-6">
                        <label class="sr-only" for="cemail">Email</label>
                        <input id="cemail" type="email" class="form-control" v-model="cemail" name="cemail" placeholder="Email" required="" aria-required="true">
                    </div>
                    <div class="col-12">
                        <select id="reason" v-model="reason" class="form-select" name="reason">
                            <option selected>Select an option you are interested in...</option>
                            <option value="full-time">Full-Time</option>
                            <option value="part-time">Part-Time</option>
                            <option value="contracting">Contracting</option>
                            <option value="consulting">Consulting</option>
                            <option value="contact">Contact Me</option>
                        </select>
                    </div>
                    <div class="col-12">
                        <label class="sr-only" for="cmessage">Your message</label>
                        <textarea class="form-control" v-model="message" name="message" placeholder="Enter your message" rows="10" required="" aria-required="true"></textarea>
                    </div>
                    <div class="form-group col-12">
                        <button type="submit" class="btn btn-block btn-primary py-2" >Send Now</button>
                    </div>                           
                </div><!--//form-row-->
            </form>
        </div><!--//container-->
    </section>
    `,

    data: function () {
        return {
            admin: 'B3PO',
            email: 'brandon@cyntax.org',
            cname: '',
            cemail: '',
            reason: '',
            message: ''
        }
    },

    methods: {
        sendForm () {
            axios.post('https://api.b3po.io/api/contacts/', {
                name: this.cname,
                email: this.cemail,
                reason: this.reason,
                message: this.message
            })
            .then(res => {
                if (res.status >= 200 && res.status <= 299){
                    alert(`Thank you, your message has been received. I will follow up within 48 hours.`);
                    this.cname, this.cemail, this.reason, this.message = '';
                    document.getElementById('cname').value = '';
                    document.getElementById('cemail').value = '';
                }
                else if (res.status >= 400 && res.status <= 499){alert(`Message Failed: Client Error ${res}`);}
                else if (res.status >= 500 && res.status <= 99){alert(`Message Failed: Server Error ${res}`);}
            })
            .catch(err => console.log(err));
        } 
    }

})