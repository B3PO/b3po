app.component('aboutme', {
    template: 
    /*html*/
    ` 
    <section class="about-me-section p-3 p-lg-5 theme-bg-light">
			<div class="container">
				<div class="profile-teaser row">
					
					<div class="col">
						<h2 class="name font-weight-bold mb-1">{{admin}}</h2>
						<div class="tagline mb-3">{{title}}</div>
						<div class="bio mb-4">{{bio}}
						</div>
						<div class="mb-4">
							<a class="btn btn-primary mb-3" href="resume.html"><i class="fas fa-file-alt me-2"></i><span class="d-none d-md-inline">View</span> Resume</a>
							<!-- Portfolio still needs to be developed.
							<a class="btn btn-secondary me-2 mb-3" href="portfolio.html"><i class="fas fa-arrow-alt-circle-right me-2"></i><span class="d-none d-md-inline">View</span> Portfolio</a>-->
							
						</div>
					</div>
					
					<div class="col-md-5 col-lg-5">
					    <img class="profile-image img-fluid mb-3 mb-lg-0 me-md-0" src="assets/images/avi-bkgd.JPG" alt="">
					</div>
				</div>
			</div>
		</section>`,
        data: function() {
            return {
                admin: 'B3PO',
                title: 'Full-Stack Software Engineer',
                bio: 'I am a software engineer with experience in front-end development and entry experience in back-end development.  Simplicity is the ultimate Sophistication. Junior Full-Stack Software Engineer.'
            }
        }
})