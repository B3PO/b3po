app.component('overview', {
    template: 
    /*html*/
    `   
    <section class="overview-section p-3 p-lg-5">
        <div class="container">
            <h2 class="section-title font-weight-bold mb-3">What I do</h2>
            <div class="section-intro mb-5">I have more than 5 years of experience building software for personal, educational, and business purposes. I started off developing websites with HTML/CSS/JS and Bootstrap. I started developing MEAN/MEVN stack applications within the last year. I have worked with Angular, Vue and React. Below is a quick overview of my technical skill set and technologies that I currently use.</div>
            <div class="row">
                <div class="item col-6 col-lg-3">
                    <div class="item-inner">
                        <div class="item-icon"><i class="fab fa-html5 me-2"></i><i class="fab fa-css3-alt me-2"></i><i class="fab fa-js-square me-2"></i><i class="fab fa-bootstrap me-2"></i></div>
                        <h3 class="item-title">HTML, CSS, JS, Bootstrap</h3>
                        <div class="item-desc">Over 5 years of front-end web development experience designing UI/UX and developing responsive sites using html, css, js and bootstrap.</div>
                    </div>
                </div>
                <div class="item col-6 col-lg-3">
                    <div class="item-inner">
                        <div class="item-icon"><i class="fab fa-react me-2"></i><i class="fab fa-vuejs me-2"></i><i class="fab fa-angular me-2"></i></div>
                        <h3 class="item-title">React, Vue, Angular</h3>
                        <div class="item-desc">Over 2 years of experience developing with front-end frameworks and libraries to create seamless client-side functionality for various needs. </div>
                    </div>
                </div>
                <div class="item col-6 col-lg-3">
                    <div class="item-inner">
                        <div class="item-icon"><i class="fab fa-node-js me-2"></i><i class="fas fa-coffee me-2" style="color: #f7df1e;"></i><i class="fas fa-database " style="color:#589636;"></i></div>
                        <h3 class="item-title">Node, Express, MongoDB</h3>
                        <div class="item-desc">Junior experience in back-end development with RESTful APIs. The past year I have converted to working with Node.js and Next.js for SSR.</div>
                    </div>
                </div>
                
                
                
                <div class="item col-6 col-lg-3">
                    <div class="item-inner">
                        <div class="item-icon"><i class="fab fa-linux me-2" style="color:black;"></i><i class="fab fa-ubuntu me-2" style="color: #dd4814;"></i><i class="fab fa-digital-ocean " style="color:#008bcf;"></i></div>
                        <h3 class="item-title">Linux, Ubuntu, DigitalOcean</h3>
                        <div class="item-desc">Over 5 years of experience configuring linux servers to deploy websites and web apps on Digitalocean. Implement security best practices. </div>
                    </div>
                </div>
            </div>
            <!--<div class="text-center py-3"><a href="services.html" class="btn btn-primary"><i class="fas fa-arrow-alt-circle-right me-2"></i>Services &amp; Pricing
                
            </a></div>-->
            
        </div>
    </section>`,
        data: function() {
            return {
                
            }
        }
})