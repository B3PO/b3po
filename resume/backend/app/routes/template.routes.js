module.exports = app => {
    const contacts = require("../controllers/template.controller.js");
    var router = require("express").Router();
    // Create a new contact
    router.post("/", contacts.create);
    app.use('/api/contacts', router);
};