module.exports = mongoose => {
    const Contact = mongoose.model(
      "contact",
      mongoose.Schema(
        {
          name: String,
          email: String,
          reason: String,
          message: String
        },
        { timestamps: false }
      )
    );
    return Contact;
  };