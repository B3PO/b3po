app.component('resume-pdf', {
    template: 
    /*html*/
    ` 
<div class="container resume-container px-3 px-lg-5">
    <article class="resume-wrapper mx-auto theme-bg-light p-5 mb-5 my-5 shadow-lg">
        <div class="resume-header">
            <div class="row align-items-center">
                <div class="resume-title col-12 col-md-6 col-lg-8 col-xl-9">
                    <h2 class="resume-name mb-0 text-uppercase">{{admin}}</h2>
                    <div class="resume-tagline mb-3 mb-md-0">{{title}}</div>
                </div>
                <div class="resume-contact col-12 col-md-6 col-lg-4 col-xl-3">
                    <ul class="list-unstyled mb-0">
                        <li class="mb-2"><i class="fas fa-phone-square fa-fw fa-lg me-2 "></i><a class="resume-link" href="tel:864-256-1502">{{phone}}</a></li>
                        <li class="mb-2"><i class="fas fa-envelope-square fa-fw fa-lg me-2"></i><a class="resume-link" href="mailto:brandon@cyntax.org">{{email}}</a></li>
                        <li class="mb-2"><i class="fas fa-globe fa-fw fa-lg me-2"></i><a class="resume-link" href="https://cyntax.org">{{website}}</a></li>
                        <li class="mb-0"><i class="fas fa-map-marker-alt fa-fw fa-lg me-2"></i>{{location}}</li>
                    </ul>
                </div>
            </div>
        </div>
        <hr>
        <div class="resume-intro py-3">
            <div class="row align-items-center">
                <div class="col-12 col-md-3 col-xl-2 text-center">
                    <img class="resume-profile-image mb-3 mb-md-0 me-md-5  ms-md-0 rounded mx-auto" src="assets/images/avi-no-bkgd.png" alt="image">
                </div>
                <div class="col text-start">
                    <p class="mb-0">{{bio}}</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="resume-body">
            <div class="row">
                <div class="resume-main col-12 col-lg-8 col-xl-9   pe-0   pe-lg-5">
                    <section class="work-section py-3">
                        <h3 class="text-uppercase resume-section-heading mb-4">Experience</h3>
                        <div class="item mb-3">
                            <div class="item-heading row align-items-center mb-2">
                                <h4 class="item-title col-12 col-md-6 col-lg-8 mb-2 mb-md-0">Full-Stack Engineer</h4>
                                <div class="item-meta col-12 col-md-6 col-lg-4 text-muted text-start text-md-end">Cyntax | Jan. 2020 - Present</div>  
                            </div>
                            <div class="item-content">
                                <p>Develop, host and maintain single page applications and full-stack web applications for multiple small businesses and projects. Create admin dashboards for companies to monitor and analyze real-time data from their business systems. Create front-end applications that interact with existing APIs like Twitter or Coinmarketcap. Create, test and update API's using Node, Express and Postman. </p>
                                <ul class="resume-list">
                                    <li>Angular, Vue 3, HTML, CSS, JS, Bootstrap</li>
                                    <li>Node, Express, Python, MongoDB</li>
                                    <li>Linux/Ubuntu, NGINX, Cloudflare, Digitalocean</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item mb-3">
                            <div class="item-heading row align-items-center mb-2">
                                <h4 class="item-title col-12 col-md-6 col-lg-8 mb-2 mb-md-0">Director of E-Commerce</h4>
                                <div class="item-meta col-12 col-md-6 col-lg-4 text-muted text-start text-md-end">Old South Trade | Apr. 2021 - Jan. 2022</div>
                            </div>
                            <div class="item-content">
                                <p>Generated $6 million in annual revenue on E-commerce in 8 months while streamlining business efficiency and productivity. Implemented an automated warehouse system that controls the process from order to fulfillment; connecting Amazon, Shopify, SOS Inventory and Quickbooks. </p>
                                <ul> 
                                    <li> Develop custom functionality for Shopify theme using Shopify's Liquid template language and JavaScript.  </li>
                                    <li> Develop custom python modules using pandas to analyze, display or manipulate data. </li>
                                    <li> Create warehouse/shipping software to track order fulfillment using python, kivy and electron.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item mb-3">
                            <div class="item-heading row align-items-center mb-2">
                                <h4 class="item-title col-12 col-md-6 col-lg-8 mb-2 mb-md-0">Web Developer</h4>
                                <div class="item-meta col-12 col-md-6 col-lg-4 text-muted text-start text-md-end">Cyntax | Jan. 2017 - Jan. 2020</div>
                            </div>
                            <div class="item-content">
                                <p>Create, host, update and maintain websites and web applications for multiple small businesses and non-profit organizations. Create LAMP & WAMP Servers using physical nodes in-house or virtual nodes using Digitalocean. Manage email, web and database servers as well as monitor, update and implement security measures and best practices.</p>
                                <ul class="resume-list">
                                    <li>HTML, CSS, JS, Bootstrap</li>
                                    <li>Linux/Ubuntu, Apache, MySQL, PHP, Digitalocean</li>
                                </ul>
                            </div>
                        </div>
                    </section>

                    <section class="work-section py-3">
                        <h3 class="text-uppercase resume-section-heading mb-4">Education</h3>
                        <div class="item mb-3">
                            <div class="item-heading row align-items-center mb-2">
                                <h4 class="item-title col-12 col-md-6 col-lg-8 mb-2 mb-md-0">University of South Carolina - Upstate</h4>
                                <div class="item-meta col-12 col-md-6 col-lg-4 text-muted text-start text-md-end">Jan. 2018 - Jul. 2021</div>
                                <h6><i>B.S. Computer Science</i></h6>
                            </div>
                            <div class="item-content">
                                <p>Vice President of Computer Science Club. Created and maintain the official Discord chat server for USC Computer Science. Courses were focused on object oriented design in Java and Python covering algorithms and data structures. Cognate courses in Information Systems Management for database design, implementation, deployment and data warehousing with SQL. </p>
                                <ul class="resume-list">
                                    <h4 class="item-title col-12 col-md-6 col-lg-8 mb-2 mb-md-0">Projects </h4><i>
                                    <li>Senior: Analyzed the speed & strength of encryption standards using Python</li>
                                    <li>Directed Research: Data Analysis and Visualization with Python, Pandas, Chart.js</li>
                                    <li>Software Engineering: Lead team of developers to create Payroll System using VB, AWS, SQL Server & Github</li>
                                    <li>Operating Systems: Lead team of developers to create a Bank Teller System using Java and a Raspberry Pi modified with touch screen</li></i>
                                </ul>
                            </div>
                        </div>
                        <div class="item mb-3">
                            <div class="item-heading row align-items-center mb-2">
                                <h4 class="item-title col-12 col-md-6 col-lg-8 mb-2 mb-md-0">Spartanburg Community College</h4>
                                <div class="item-meta col-12 col-md-6 col-lg-4 text-muted text-start text-md-end">May 2017</div>
                                <h6><i>Certificate: Software Development & Database Administration</i></h6>
                            </div>
                            <div class="item-content">
                                <p>Completed certification that covers all senior level network, programming and database courses for A.S. in Computer Technology - Programming. Senior Project: Created a financial tracker where users enter their monthly bills and income to get financial guidance using VB and SQL Server. Hosted on Windows IIS Server 2016.</p>
                                <ul class="resume-list">
                                    <li>SQL Server</li>
                                    <li>C++, Visual Basic, C#</li>
                                    <li>Swift - iOS App Development</li>
                                    <li>Windows Server 2008 R2/ 2016</li>
                                </ul>
                            </div>
                        </div>
                    </section>

                    <section class="project-section py-3">
                        <h3 class="text-uppercase resume-section-heading mb-4">Projects</h3>
                        <div class="item mb-3">
                            <div class="item-heading row align-items-center mb-2">
                                <h4 class="item-title col-12 col-md-6 col-lg-8 mb-2 mb-md-0">HoDL - Star Fall Legion</h4>
                                <div class="item-meta col-12 col-md-6 col-lg-4 text-muted text-start text-md-end">NFT Project | Mar. 2022 - Present</div>
                            </div>
                            <div class="item-content">
                                <p>Research and implement ERC-721 NFT using Solidity and JavaScript/Node for auto-collection creation. </p>
                            </div>
                        </div>
                        <div class="item mb-3">
                            <div class="item-heading row align-items-center mb-2">
                                <h4 class="item-title col-12 col-md-6 col-lg-8 mb-2 mb-md-0">HoDL - Sakura</h4>
                                <div class="item-meta col-12 col-md-6 col-lg-4 text-muted text-start text-md-end">NFT Project | Nov. 2021 - Present</div>
                                <a href="https://hodl-sakura.io" style="text-decoration: none;">hodl-sakura.io</a>
                            </div>
                            <div class="item-content">
                                <p>Create bootstrap responsive website based on mockup from creative director. Configure server and cloudflare for hosting and security. Create Vue 3 front-end story game based on demo game developed using Twinery. Develop back-end for gameplay, player registration/creation integrated with metamask, and game metrics. Create back-end API using Node, Express, Mongoose and MongoDB to store player data. Research and implement ERC-721 NFT for game play passes and authenticate with metamask.</p>
                            </div>
                        </div>
                    </section>
                </div>
                <aside class="resume-aside col-12 col-lg-4 col-xl-3 px-lg-4 pb-lg-4">
                    <section class="skills-section py-3">
                        <h3 class="text-uppercase resume-section-heading mb-4">Skills</h3>
                        <div class="item">
                            <h4 class="item-title">Front-End</h4>
                                <ul class="list-unstyled resume-skills-list">
                                    <li class="mb-2">HTML, CSS, Bootstrap</li>
                                    <li class="mb-2">CSS Animation</li>
                                    <li class="mb-2">UI/UX Design & Development</li>
                                    <li class="mb-2">JavaScript, Angular, Vue</li>
                                    <li class="mb-2">Vue 3, Vuex, Vue Router, Axios</li>
                                </ul>
                            </div>
                            <div class="item">
                                <h4 class="item-title">Back-End</h4>
                                <ul class="list-unstyled resume-skills-list">
                                    <li class="mb-2">MySQL, SQL Server</li>
                                    <li class="mb-2">Node, Express, MongoDB, Mongoose</li>
                                    <li class="mb-2">REST API & Microservices</li>
                                    <li class="mb-2">Algorithms and data structures</li>
                                    </ul>
                                </div>
                                <div class="item">
                                <h4 class="item-title">Tools</h4>
                                    <ul class="list-unstyled resume-skills-list">
                                        <li class="mb-2">Postman </li>
                                        <li class="mb-2">Vi/Vim, Nano </li>
                                        <li class="mb-2">Git, Github, Gitlab, Gitlab CI/CD </li>
                                        <li class="mb-2">Bash Scripts, MakeFiles & Docker Containers for Automation</li>
                                    </ul>
                                </div>
                            </section>
                            <section class="education-section py-3">
                                <h3 class="text-uppercase resume-section-heading mb-4">Education</h3>
                                <ul class="list-unstyled resume-education-list">
                                    <li class="mb-3">
                                        <div class="resume-degree font-weight-bold">B.S. Computer Science</div>
                                        <div class="resume-degree-org text-muted">University of South Carolina - Upstate</div>
                                        <div class="resume-degree-time text-muted">2018 - 2021</div>
                                    </li>
                                    <li>
                                        <div class="resume-degree font-weight-bold">Certificate - Software Development & Database Administration</div>
                                        <div class="resume-degree-org text-muted">Spartanburg Community College</div>
                                        <div class="resume-degree-time text-muted">2017</div>
                                    </li>
                                </ul>
                            </section>
                            <section class="skills-section py-3">
                                <h3 class="text-uppercase resume-section-heading mb-4">Interests</h3>
                                <ul class="list-unstyled resume-interests-list mb-0">
                                    <li class="mb-2">Family Time</li>
                                    <li class="mb-2">Bitcoin</li>
                                    <li class="mb-2">Traveling</li>
                                    <li class="mb-2">Hiking/Outdoors</li>
                                    <li class="mb-2">Web/Web3 Development</li>
                                    <li class="mb-2">Raspberry Pi, Bots/Robots</li>
                                </ul>
                            </section>
                        </aside>
                    </div>
                </div>
                <hr>
                <div class="resume-footer text-center">
                    <ul class="resume-social-list list-inline mx-auto mb-0 d-inline-block text-muted">
                        <li class="list-inline-item mb-lg-0 me-3"><a class="resume-link" href="https://gitlab.com/B3PO"><i class="fab fa-gitlab fa-2x me-2" data-fa-transform="down-4"></i><span class="d-none d-lg-inline-block text-muted">Gitlab</span></a></li>
                        <li class="list-inline-item mb-lg-0 me-3"><a class="resume-link" href="https://github.com/brandon-rowe"><i class="fab fa-github-square fa-2x me-2" data-fa-transform="down-4"></i><span class="d-none d-lg-inline-block text-muted">Github</span></a></li>                        
                        <li class="list-inline-item mb-lg-0 me-3"><a class="resume-link" href="https://linkedin.com/in/brandon-rowe-56887791/"><i class="fab fa-linkedin fa-2x me-2" data-fa-transform="down-4"></i><span class="d-none d-lg-inline-block text-muted">Linkedin</span></a></li>
                        <li class="list-inline-item mb-lg-0 me-lg-3"><a class="resume-link" href="https://twitter.com/B3p01"><i class="fab fa-twitter-square fa-2x me-2" data-fa-transform="down-4"></i><span class="d-none d-lg-inline-block text-muted">Twitter</span></a></li>
                    </ul>
                </div>
            </article>
        </div>`,
        data: function() {
            return {
                admin: 'B3PO',
                title: 'Full-Stack Software Engineer',
                bio: 'I am a software engineer specialized in front-end, back-end & API development. I have worked with computers and websites since I was a teenager. I enjoy spending time with my family and mountain hiking/climbing. I love open-source projects, bitcoin and the internet. I like to keep things simple so I host my own resume, business-card and website to centralize my online content. Outside of that, I use Gitlab and Twitter regularly. Simplicity is the ultimate Sophistication. Junior Full-Stack Software Engineer.',
                email: 'brandon@cyntax.org',
                phone: '864-256-1502',
                website: 'www.cyntax.org',
                location: 'Greenville, SC'
            }
        }
})