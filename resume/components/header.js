app.component('navbar', {
    
    template: 
    /*html*/
    `   <header class="header text-center" >	    
    <div class="force-overflow" >
      <h1 class="blog-name pt-lg-4 mb-0"><a class="no-text-decoration" href="index.html">{{admin}}</a></h1>
      <nav class="navbar navbar-expand-lg navbar-dark" >
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div id="navigation" class="collapse navbar-collapse flex-column" >
          <div class="profile-section pt-3 pt-lg-0">
            <img class="profile-image mb-3 rounded-circle mx-auto" src="assets/images/avi-no-bkgd.png" alt="image" >
            <div class="bio mb-3">{{intro}}</div>
            <ul class="social-list list-inline py-2 mx-auto">
              <li class="list-inline-item"><a href="https://twitter.com/B3p01"><i class="fab fa-twitter fa-fw"></i></a></li>
              <li class="list-inline-item"><a href="https://www.linkedin.com/in/brandon-rowe-56887791/"><i class="fab fa-linkedin-in fa-fw"></i></a></li>
              <li class="list-inline-item"><a href="https://gitlab.com/B3PO"><i class="fab fa-gitlab fa-fw"></i></a></li>
              <li class="list-inline-item"><a href="https://github.com/brandon-rowe"><i class="fab fa-github-alt fa-fw"></i></a></li>
            </ul>
            <hr> 
          </div>
          
          <ul class="navbar-nav flex-column text-start">
            <li class="nav-item">
              <a class="nav-link" href="index.html"><i class="fas fa-user fa-fw me-2"></i>About Me<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="resume.html"><i class="fas fa-file-alt fa-fw me-2"></i>Resume</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contact.html"><i class="fas fa-envelope-open-text fa-fw me-2"></i>Contact</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="https://b3po.io/business-card"><i class="fas fa-address-card fa-fw me-2"></i>Business Card</a>
            </li>
            
          </ul>
        </div>
      </nav>
    </div>
  </header>`,
        data: function() {
            return {
                admin: 'B3PO',
                intro: 'Hi, my name is B3PO and I am a software engineer. Welcome to my professional contact site. Please, have a look around.'
            }
        }
})