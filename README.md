# B3PO

For the public

## Getting started

Running B3PO.io homepage

```
cd programing-folder/
git clone https://gitlab.com/B3PO/B3PO
cd B3PO/
npm install && npm run serve
```

# resume backend

## Getting started

Before running npm install && npm start, open db.config.js in app folder (B3PO/resume/backend/app/config/db.config.js) and replace the URL connection string with the connection string on your mongodb.

Data Format
```
"name":"name",
"email":"email",
"reason":"reason",
"message":"message"
```

## Start backend

```
cd programing-folder/
git clone https://gitlab.com/B3PO/B3PO
cd B3PO/resume/backend
npm install && npm start
```

Open frontend-testing folder and open index.html for local testing.
* CORS extension needed for local testing.
