const db = require("../models");
const Contact = db.contacts;
// Create and Save a new demo
exports.create = (req, res) => {
// Validate request
  if (!req.body.name) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }
  // Create a demo
  const contact = new Contact({
    name: req.body.name,
    email: req.body.email,
    reason: req.body.reason,
    message: req.body.message
  });
  // Save demo in the database
  contact
    .save(contact)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the demo."
      });
    });
};