app.component('resume', {
    template: 
    /*html*/
    ` 
    <section class="cta-section theme-bg-light py-5">
        <div class="container text-center single-col-max-width">
            <h2 class="heading mb-3">Online Resume</h2>
            <a class="btn btn-primary" :href="'./assets/b3po_io.pdf'" target="_blank"><i class="fas fa-file-pdf me-2"></i>Download PDF Version</a>
        </div>
    </section>
<div class="container resume-container px-3 px-lg-5">
    <article class="resume-wrapper mx-auto theme-bg-light p-5 mb-5 my-5 shadow-lg">
        <div class="resume-header">
            <div class="row align-items-center">
                <div class="resume-title col-12 col-md-6 col-lg-8 col-xl-9">
                    <h2 class="resume-name mb-0 text-uppercase">{{admin}}</h2>
                    <div class="resume-tagline mb-3 mb-md-0">{{title}}</div>
                </div>
                <div class="resume-contact col-12 col-md-6 col-lg-4 col-xl-3">
                    <ul class="list-unstyled mb-0">
                        <li class="mb-2"><i class="fas fa-phone-square fa-fw fa-lg me-2 "></i><a class="resume-link" href="tel:864-256-1502">{{phone}}</a></li>
                        <li class="mb-2"><i class="fas fa-envelope-square fa-fw fa-lg me-2"></i><a class="resume-link" href="mailto:brandon@cyntax.org">{{email}}</a></li>
                        <li class="mb-2"><i class="fas fa-globe fa-fw fa-lg me-2"></i><a class="resume-link" href="https://cyntax.org">{{website}}</a></li>
                        <li class="mb-0"><i class="fas fa-map-marker-alt fa-fw fa-lg me-2"></i>{{location}}</li>
                    </ul>
                </div>
            </div>
        </div>
        <hr>
        <div class="resume-intro py-3">
            <div class="row align-items-center">
                <div class="col-12 col-md-3 col-xl-2 text-center">
                    <img class="resume-profile-image mb-3 mb-md-0 me-md-5  ms-md-0 rounded mx-auto" src="assets/images/avi-no-bkgd.png" alt="image">
                </div>
                <div class="col text-start">
                    <p class="mb-0">{{bio}}</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="resume-body">
            <div class="row">
                <div class="resume-main col-12 col-lg-8 col-xl-9   pe-0   pe-lg-5">
                    <section class="work-section py-3">
                        <h3 class="text-uppercase resume-section-heading mb-4">Experience</h3>
                        <div class="item mb-3">
                            <div class="item-heading row align-items-center mb-2">
                                <h4 class="item-title col-12 col-md-6 col-lg-8 mb-2 mb-md-0">Full-Stack Engineer</h4>
                                <div class="item-meta col-12 col-md-6 col-lg-4 text-muted text-start text-md-end">Cyntax | Jan. 2022 - Present</div>
                                <a href="https://cyntax.io" style="text-decoration: none;">cyntax.io</a>  
                            </div>
                            <div class="item-content">
                                <p>Develop, host and maintain single page applications and full-stack web applications for multiple small businesses and projects. Create admin dashboards for companies to monitor and analyze real-time data from their business systems. Create, test and update API's using Node, Express and Postman. </p>
                                <ul class="resume-list">
                                    <li>Vue 3, HTML, CSS, JS, Bootstrap</li>
                                    <li>Node, Express, MongoDB</li>
                                    <li>Linux/Ubuntu, NGINX, Cloudflare, Docker, Gitlab CI/CD, Digitalocean</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item mb-3">
                            <div class="item-heading row align-items-center mb-2">
                                <h4 class="item-title col-12 col-md-6 col-lg-8 mb-2 mb-md-0">Director of E-Commerce</h4>
                                <div class="item-meta col-12 col-md-6 col-lg-4 text-muted text-start text-md-end">Old South Trade | Apr. 2021 - Jan. 2022</div>
                                <a href="https://oldsouthtrade.com" style="text-decoration: none;">oldsouthtrade.com</a>
                            </div>
                            <div class="item-content">
                                <p>Generated $6 million in annual revenue on E-commerce in 8 months while streamlining business efficiency and productivity. Implemented an automated warehouse system that controls the process from order to fulfillment; connecting Amazon, Shopify, SOS Inventory and Quickbooks. </p>
                                <ul> 
                                    <li> Develop custom functionality for Shopify theme using Shopify's Liquid template language and JavaScript.  </li>
                                    <li> CRM, Marketing, yada yada </li>
                                </ul>
                            </div>
                        </div>
                        <div class="item mb-3">
                            <div class="item-heading row align-items-center mb-2">
                                <h4 class="item-title col-12 col-md-6 col-lg-8 mb-2 mb-md-0">Front-End Developer</h4>
                                <div class="item-meta col-12 col-md-6 col-lg-4 text-muted text-start text-md-end">Cyntax | Jan. 2020 - Apr. 2021</div><a href="https://cyntax.org" style="text-decoration: none;">cyntax.org</a>
                            </div>
                            <div class="item-content">
                                <p>Create, host, update and maintain websites and web applications for multiple small businesses and non-profit organizations. Create and manage servers using Digitalocean.</p>
                            </div>
                        </div>
                    </section>

                    <section class="work-section py-3">
                        <h3 class="text-uppercase resume-section-heading mb-4">Education</h3>
                        <div class="item mb-3">
                            <div class="item-heading row align-items-center mb-2">
                                <h4 class="item-title col-12 col-md-6 col-lg-8 mb-2 mb-md-0">University of South Carolina - Upstate</h4>
                                <div class="item-meta col-12 col-md-6 col-lg-4 text-muted text-start text-md-end">Jan. 2018 - Jul. 2021</div>
                                <h6><i>B.S. Computer Science</i></h6>
                            </div>
                            <div class="item-content">
                                <p>Vice President of Computer Science Club. Courses were focused on object oriented design, algorithms and data structures using Java. Cognate courses in Information Systems Management for database design, implementation and data warehousing with SQL. </p>
                            </div>
                        </div>
                        <div class="item mb-3">
                            <div class="item-heading row align-items-center mb-2">
                                <h4 class="item-title col-12 col-md-6 col-lg-8 mb-2 mb-md-0">Spartanburg Community College</h4>
                                <div class="item-meta col-12 col-md-6 col-lg-4 text-muted text-start text-md-end">May 2017</div>
                                <h6><i>Certificate: Software Development & Database Administration</i></h6>
                            </div>
                            <div class="item-content">
                                <p>Senior Project: Financial Tracker - users enter their monthly bills and income to get financial guidance using VB and SQL Server. Hosted on Windows IIS Server 2016.</p>
                            </div>
                        </div>
                    </section>

                    <section class="project-section py-3">
                        <h3 class="text-uppercase resume-section-heading mb-4">Projects</h3>
                        <div class="item mb-3">
                            <div class="item-heading row align-items-center mb-2">
                                <h4 class="item-title col-12 col-md-6 col-lg-8 mb-2 mb-md-0">HoDL - Sakura</h4>
                                <div class="item-meta col-12 col-md-6 col-lg-4 text-muted text-start text-md-end">NFT Project | Nov. 2021 - May 2022</div>
                                <a href="https://hodl-sakura.io" style="text-decoration: none;">hodl-sakura.io</a>
                            </div>
                            <div class="item-content">
                                <p>Create bootstrap responsive website based on mockup from creative director. Configure server and cloudflare for hosting and security. Create Vue 3 front-end story game based on demo game developed using Twinery.</p>
                            </div>
                        </div>
                    </section>
                </div>
                <aside class="resume-aside col-12 col-lg-4 col-xl-3 px-lg-4 pb-lg-4">
                    <section class="skills-section py-3">
                        <h3 class="text-uppercase resume-section-heading mb-4">Skills</h3>
                        <div class="item">
                            <h4 class="item-title">Front-End</h4>
                                <ul class="list-unstyled resume-skills-list">
                                    <li class="mb-2">HTML, CSS, JS, Bootstrap</li>
                                    <li class="mb-2">UI/UX Design & Development</li>
                                    <li class="mb-2">Vue 3, Vuex, Vue Router, Axios</li>
                                </ul>
                            </div>
                            <div class="item">
                                <h4 class="item-title">Back-End</h4>
                                <ul class="list-unstyled resume-skills-list">
                                    <li class="mb-2">Node, Express, Nuxt, ThreeJS</li>
                                    <li class="mb-2">MongoDB, Mongoose</li>
                                    <li class="mb-2">Docker & Bash Scripting for Automation</li>
                                    </ul>
                                </div>
                                <div class="item">
                                <h4 class="item-title">Tools</h4>
                                    <ul class="list-unstyled resume-skills-list">
                                        <li class="mb-2">Postman, JTest </li>
                                        <li class="mb-2">VS Code, Vi/Vim, Nano </li>
                                        <li class="mb-2">Git, Gitlab CI/CD </li>
                                    </ul>
                                </div>
                            </section>
                            <section class="education-section py-3">
                                <h3 class="text-uppercase resume-section-heading mb-4">Education</h3>
                                <ul class="list-unstyled resume-education-list">
                                    <li class="mb-3">
                                        <div class="resume-degree font-weight-bold">B.S. Computer Science</div>
                                        <div class="resume-degree-org text-muted">University of South Carolina - Upstate</div>
                                        <div class="resume-degree-time text-muted">2018 - 2021</div>
                                    </li>
                                    <li>
                                        <div class="resume-degree font-weight-bold">Certificate - Software Development & Database Administration</div>
                                        <div class="resume-degree-org text-muted">Spartanburg Community College</div>
                                        <div class="resume-degree-time text-muted">2017</div>
                                    </li>
                                </ul>
                            </section>
                            <section class="skills-section py-3">
                                <h3 class="text-uppercase resume-section-heading mb-4">Interests</h3>
                                <ul class="list-unstyled resume-interests-list mb-0">
                                    <li class="mb-2">Time with Family</li>
                                    <li class="mb-2">Junior Pilot</li>
                                    <li class="mb-2">USA Triathlete</li>
                                    <li class="mb-2">Bitcoin</li>
                                    <li class="mb-2">Traveling</li>
                                    <li class="mb-2">Hiking/Camping</li>
                                    <li class="mb-2">Running/Biking/Swimming</li>
                                </ul>
                            </section>
                        </aside>
                    </div>
                </div>
                <hr>
                <div class="resume-footer text-center">
                    <ul class="resume-social-list list-inline mx-auto mb-0 d-inline-block text-muted">
                        <li class="list-inline-item mb-lg-0 me-3"><a class="resume-link" href="https://gitlab.com/B3PO"><i class="fab fa-gitlab fa-2x me-2" data-fa-transform="down-4"></i><span class="d-none d-lg-inline-block text-muted">Gitlab</span></a></li>
                        <li class="list-inline-item mb-lg-0 me-3"><a class="resume-link" href="https://github.com/brandon-rowe"><i class="fab fa-github-square fa-2x me-2" data-fa-transform="down-4"></i><span class="d-none d-lg-inline-block text-muted">Github</span></a></li>                        
                        <li class="list-inline-item mb-lg-0 me-3"><a class="resume-link" href="https://linkedin.com/in/brandon-rowe-56887791/"><i class="fab fa-linkedin fa-2x me-2" data-fa-transform="down-4"></i><span class="d-none d-lg-inline-block text-muted">Linkedin</span></a></li>
                        <li class="list-inline-item mb-lg-0 me-lg-3"><a class="resume-link" href="https://twitter.com/B3p01"><i class="fab fa-twitter-square fa-2x me-2" data-fa-transform="down-4"></i><span class="d-none d-lg-inline-block text-muted">Twitter</span></a></li>
                    </ul>
                </div>
            </article>
        </div>`,
        data: function() {
            return {
                admin: 'B3PO',
                title: 'Junior Full-Stack Engineer',
                bio: 'I am an avid outdoorsman who loves spending time with family. I was a parent while obtaining my education and worked tirelessly to obtain my skillset in addition to being a parent, student and employee. Junior Full-Stack Software Engineer. Simplicity is the ultimate Sophistication. ',
                email: 'brandon.rowe@cyntax.io',
                phone: '864-256-1502',
                website: 'www.cyntax.org',
                location: 'Greenville, SC'
            }
        }
})