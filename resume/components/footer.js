app.component('footer-js', {
    template: 
    /*html*/
    ` 
    <footer class="footer text-center py-4">
			<small class="copyright">Copyright &copy; <a href="https://cyntax.org/" target="_blank">Cyntax</a></small>
	</footer>`
})