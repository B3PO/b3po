// GET ALL REQUEST -- Works
function getAll() {
  axios({
    method: 'get',
    url: 'http://localhost:8080/api/contacts/'
  })
  .then(res => showOutput(res))
  .catch(err => console.log(err));
}

// GET SINGLE REQUEST -- Works
function getById() {
  let input = document.getElementById('valId').value;
  if (input === null || input === ''){
    alert('Please enter valid id.');
  }
  else {
    axios({
      method: 'get',
      url: `http://localhost:8080/api/contacts/${input}`
    })
    .then(res => showOutput(res))
    .catch(err => console.log(err));  
  }
  
}


// POST REQUEST
function post() {
  //console.log('Post Request');
  let name = document.getElementById('name').value;
  let email = document.getElementById('email').value;
  let reason = document.getElementById('reason').value;
  let message = document.getElementById('message').value;
  axios
    .post('http://localhost:8080/api/contacts/', {
      name: name,
      email: email,
      reason: reason,
      message: message
    })
    .then(res => showOutput(res))
    .catch(err => console.log(err));

}

// Show output in browser
function showOutput(res) {
  document.getElementById('res').innerHTML = `
  <div class="card card-body mb-4">
    <h5>Status: ${res.status}</h5>
  </div>

  <div class="card mt-3">
    <div class="card-header">
      Headers
    </div>
    <div class="card-body">
      <pre style="color:white">${JSON.stringify(res.headers, null, 2)}</pre>
    </div>
  </div>

  <div class="card mt-3">
    <div class="card-header">
      Data
    </div>
    <div class="card-body">
      <pre style="color:white">${JSON.stringify(res.data, null, 2)}</pre>
    </div>
  </div>

  <div class="card mt-3">
    <div class="card-header">
      Config
    </div>
    <div class="card-body">
      <pre style="color:white">${JSON.stringify(res.config, null, 2)}</pre>
    </div>
  </div>
`;
}

// Event listeners

document.getElementById('getAll').addEventListener('click', getAll);
document.getElementById('get').addEventListener('click', getById);
document.getElementById('post').addEventListener('click', post);